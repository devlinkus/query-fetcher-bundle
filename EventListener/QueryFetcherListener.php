<?php
/**
 * Created by PhpStorm.
 * User: linkus
 * Date: 07/09/18
 * Time: 16:35
 */

namespace Linkus\QueryFetcherBundle\EventListener;

use Linkus\QueryFetcherBundle\Controller\Annotation\QueryFetcher;
use Linkus\QueryFetcherBundle\Controller\Annotation\QueryReader;
//use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class QueryFetcherListener
{
    private $reader;
    private $queryReader;

    public function __construct(CachedReader $reader, QueryReader $queryReader)
    {

        $this->reader = $reader;
        $this->queryReader = $queryReader;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) {
            return;
        }
        list($controller, $methodName) = $controllers;

        $methodAnnotation = $this->reader
            ->getMethodAnnotations(new \ReflectionMethod($controller, $methodName), QueryFetcher::class);
        $requestQuery = $event->getRequest()->query->all();

        if (!($methodAnnotation || $requestQuery)) {
            return;
        }

        $this->queryReader->loadData($methodAnnotation, $requestQuery);

    }
}