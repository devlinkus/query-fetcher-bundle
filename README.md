# query-fetcher



## Install

```bash
composer require linkus/query-fetcher-bundle
```

## Usage

```php
<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Linkus\QueryFetcherBundle\Controller\Annotation\QueryFetcher;
use Linkus\QueryFetcherBundle\Controller\Annotation\QueryReader;

class DefaultController extends AbstractController
{ 
    /**
     * @Route("/")
     * @QueryFetcher("direction", requirements="asc|desc", default="asc", strict="true")
     * @QueryFetcher("sort", requirements="id|path", default="id")
     */
    public function index(QueryReader $params)
    {
        /*
          array (
             'direction' => 'asc',
             'sort' => 'id',
           )
         */
        $params->all();
    }
}
```
