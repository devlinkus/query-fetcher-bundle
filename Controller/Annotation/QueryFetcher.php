<?php
/**
 * Created by PhpStorm.
 * User: linkus
 * Date: 07/09/18
 * Time: 16:31
 */

namespace Linkus\QueryFetcherBundle\Controller\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
class QueryFetcher extends Annotation
{
    public $default;
    public $requirements;
    public $strict;
}