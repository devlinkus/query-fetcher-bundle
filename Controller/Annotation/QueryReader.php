<?php
//
namespace Linkus\QueryFetcherBundle\Controller\Annotation;

use Linkus\QueryFetcherBundle\Controller\Annotation\QueryFetcher;

class QueryReader
{
    protected $paramsConfig = [];
    protected $requestParam = [];

    protected $all = [];


    public function loadData(array $rules, array $request)
    {
        $this->loadConfig($rules);
        $this->requestParam = $request;
        $this->testParam();
    }

    public function all()
    {
        return $this->all;
    }

    public function get($name)
    {
        return $this->all[$name] ?? null;
    }

    protected function testParam()
    {
        foreach ($this->paramsConfig as $name => $cfg) {

            if (array_key_exists($name, $this->requestParam)) {
                $value = $this->requestParam[$name];
                if (!empty($cfg['requirements'])) {
                    $match = null;
                    preg_match('#^' . $cfg['requirements'] . '$#', $value, $match);
                    if (isset($match[0]) && $match[0] == $value) {
                        $this->all[$name] = $value;
                    } else {
                        if($cfg['strict'] === true)
                        {
                            $msg = 'Request parameter "'.$name.'" is not valid. Requirement: "'.$cfg['requirements'].'".';
                            throw new \Symfony\Component\Routing\Exception\InvalidParameterException($msg, 400);
                        }
                        $this->all[$name] = !empty($cfg['default']) ? $cfg['default'] : null;
                    }
                } else {
                    $this->all[$name] = $value;
                }
            } else {
                $this->all[$name] = $cfg['default'];
            }
        }
    }

    protected function loadConfig(array $data)
    {
        foreach ($data as $item) {
            if ($item instanceof QueryFetcher && $this->hasConfig($item)) {
                $this->paramsConfig[$item->value] = [
                    'requirements' => $item->requirements,
                    'default' => $item->default,
                    'strict' => $item->strict ? true : false
                ];
            }
        }

    }


    protected function hasConfig(QueryFetcher $config)
    {
        return $config->value ? true : false;
    }
}